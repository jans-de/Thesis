-- create new database
CREATE DATABASE IF NOT EXISTS `pdg_dev`;
USE `pdg_dev`;

-- disabled foreign key constraint violations which prohibits table deletion
SET FOREIGN_KEY_CHECKS = 0;

-- delete tables
DROP TABLE IF EXISTS `Studies`;
DROP TABLE IF EXISTS `Forms`;
DROP TABLE IF EXISTS `LookupClasses`;
DROP TABLE IF EXISTS `Fields`;
DROP TABLE IF EXISTS `FieldAttributes`;
DROP TABLE IF EXISTS `Relations`;

DROP VIEW  IF EXISTS `FullNames`;

-- re-enable it
SET FOREIGN_KEY_CHECKS = 1;

-- (re-)create tables
CREATE TABLE IF NOT EXISTS `Studies` (
    id INT NOT NULL AUTO_INCREMENT,
    systemName VARCHAR(64) NOT NULL,
    name VARCHAR(64) NOT NULL,
    title VARCHAR(64),
    version VARCHAR(16),
    PRIMARY KEY (id),
    CONSTRAINT Studies_UKey1 UNIQUE (systemName , name , version)
);

CREATE TABLE IF NOT EXISTS `Forms` (
    id INT NOT NULL AUTO_INCREMENT,
    studyId INT,
    name VARCHAR(32),
    type VARCHAR(16),
    PRIMARY KEY (id),
    CONSTRAINT Forms_FK1 FOREIGN KEY (studyId)
        REFERENCES Studies (id),
    CONSTRAINT Forms_UKey1 UNIQUE (studyId , name)
);

CREATE TABLE IF NOT EXISTS `LookupClasses` (
    id INT NOT NULL AUTO_INCREMENT,
    classId INT NOT NULL,
    name VARCHAR(64),
    entry VARCHAR(64),
    value VARCHAR(64),
    PRIMARY KEY (id)
);
CREATE INDEX idx_LookupClasses_lookupClassId ON LookupClasses(classId);


CREATE TABLE IF NOT EXISTS `Fields` (
    id INT NOT NULL AUTO_INCREMENT,
    formId INT,
    lookupClassId INT,
    name VARCHAR(64) NOT NULL,
	buildType VARCHAR(64),
    mandatory ENUM('error', 'warning', 'optional', 'errorConditional', 'warningConditional', 'optionalConditional') NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT Fields_FK1 FOREIGN KEY (formId)
        REFERENCES Forms (id),
	CONSTRAINT Fields_FK2 FOREIGN KEY (lookupClassId)
        REFERENCES LookupClasses (classId),
    CONSTRAINT Fields_UKey1 UNIQUE (formId , name)
);

CREATE TABLE IF NOT EXISTS `FieldAttributes` (
    id INT NOT NULL AUTO_INCREMENT,
    fieldId INT,
    attribute VARCHAR(64) NOT NULL,
    value VARCHAR(64),
    PRIMARY KEY (id),
    CONSTRAINT FieldPropEntities_FK1 FOREIGN KEY (fieldId)
        REFERENCES Fields (id),
    CONSTRAINT FieldPropEntities_UKey1 UNIQUE (fieldId , attribute)
);

CREATE TABLE IF NOT EXISTS `Relations` (
    id INT NOT NULL AUTO_INCREMENT,
    fromFieldId INT NOT NULL,
    toFieldId INT NOT NULL,
    relationType VARCHAR(64),
    onValue VARCHAR(64),
    PRIMARY KEY (id),
    CONSTRAINT Relations_FK1 FOREIGN KEY (fromFieldId)
        REFERENCES Fields (id),
    CONSTRAINT Relations_FK2 FOREIGN KEY (toFieldId)
        REFERENCES Fields (id),
    CONSTRAINT Relations_UKey1 UNIQUE (fromFieldId , toFieldId)
);

CREATE OR REPLACE VIEW FullNames AS
    SELECT 
        stud.id as studyId,
        stud.version as version,
        form.id as formId,
        Fields.id as fieldId,
        Fields.name as name,
        CONCAT("alcedis/", stud.systemName, '/', stud.name, '/', stud.version, '/', form.name, '/', Fields.name) AS identifier
    FROM Fields
	JOIN Forms form ON form.id = Fields.formId
    JOIN Studies stud ON stud.id = form.studyId;

-- create index
CREATE INDEX idx_Fields_formId ON Fields(formId);
CREATE INDEX idx_FieldAttributes_fieldId ON FieldAttributes(fieldId);
CREATE INDEX idx_Relations_fromFieldId ON Relations(fromFieldId);
CREATE INDEX idx_Relations_toFieldId ON Relations(toFieldId);
