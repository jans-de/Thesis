# README

Bachelorthesis "Vergleich zwischen relationalen und Graphdatenbanken zur Speicherung
von synthetischen Patientendaten" zur Erlangung des akademischen Grades **Bachelor of Science**
eingereicht im Fachbereich Mathematik, Naturwissenschaften und Informatik an der
Technischen Hochschule Mittelhessen von Jan Henrik Steiner am 19. Juni 2023.

Copyright: CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)

----

Bachelorthesis "Vergleich zwischen relationalen und Graphdatenbanken zur Speicherung
von synthetischen Patientendaten" (translation: "Comparison between relational and graph databases for storing
of synthetic patient data") to obtain the academic degree **Bachelor of Science**.
submitted to the Department of Mathematics, Natural Sciences and Computer Science at the
Technische Hochschule Mittelhessen by Jan Henrik Steiner on June 19, 2023.

Copyright: CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
