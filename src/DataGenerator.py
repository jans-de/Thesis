import random
from datetime import datetime, timedelta

from nodes.Node import Node


class DataGenerator:
    RANGE_BETWEEN_DATES = 7

    def __init__(self):
        self.dates = {}

    def generate_value(self, node: Node, **kwargs):
        if node is None:
            return

        # save the node name + "_<iteration>" for the self.dates
        node_name = node.get_name()
        lookup = kwargs.get("lookup", {})
        it = kwargs.get("iteration")

        if it is not None:
            node_name = node_name + "_" + str(it)
        build_type = node.get_build_type()
        val = None

        # integers and floats
        if build_type == "afForm_Widget_Integer" or \
                build_type == "afForm_Widget_Float":
            lower = node.get_min()
            upper = node.get_max()
            if build_type == "afForm_Widget_Integer":
                # random int value between lower <= x <= upper
                val = random.randint(lower, upper)
            elif build_type == "afForm_Widget_Float":
                # a random float value between lower <= x <= upper, rounded to 3 decimals
                val = round(random.uniform(lower, upper), 3)

        # lookups, checkboxes and radio buttons
        elif build_type == "afForm_Widget_Lookup":
            val = random.choice(list(lookup.get_definitions().values()))
        elif build_type == "afForm_Widget_Radio":
            val = random.choice(list(lookup.get_definitions().values()))
        elif build_type == "afForm_Widget_Checkbox":
            choice = random.random()
            if choice >= 0.5:
                val = "FILLED"
            else:
                val = "EMPTY"

        # date, time and datetime
        elif build_type == "afForm_Widget_Date" or \
                build_type == "afForm_Widget_Time" or \
                build_type == "afForm_Widget_DateTime":
            """Caches the dates, times and datetimes because we want them to be chronologically.
            Generating them on the fly, can't be done reliably without assumptions."""
            field_name = node.get_id().split("/")[-2] + node_name
            self.dates[field_name] = "date"
            val = "date"  # placeholder value, which is overwritten in generate_dates()

        # text
        elif build_type == "afForm_Widget_Text":
            val = "Lorem ipsum"
        else:
            val = "n/a"

        return val

    def generate_dates(self) -> dict[str, str]:
        """
        Generates values for every date, which was previously cached in self.dates .
        First of all, the earliest possible date is determined by
        (number of dates * self.RANGE_BETWEEN_DATES) + 1 which is saved in prev_date.
        Then a random number of days (1<= d <= self.RANGE_BETWEEN_DATES) is added to prev_date.
        This date becomes the new prev_date.
        Finally, a dictionary {field_identifier: date as string} is returned

        :return: a dictionary {field_identifier: date as string}
        """
        first_date = datetime.today() - timedelta(days=((len(self.dates) * self.RANGE_BETWEEN_DATES) + 1))
        prev_date = first_date
        for k, v in self.dates.items():
            self.dates[k] = prev_date.strftime("%Y-%m-%d")
            prev_date = prev_date + timedelta(days=(random.randint(1, self.RANGE_BETWEEN_DATES)))
        return self.dates
