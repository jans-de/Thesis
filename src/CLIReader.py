import sys


class CLIReader:
    valid_arguments = {
        "-h": "Prints the usage",
        "--help": "Prints the usage",
        "-v": "Prints the version",
        "--version": "Prints the version",
        "-d": "Starts debug mode",
        "--debug": "Starts debug mode",
        "--db": "which database to use",
        "--file": "Path to ADL file",
        "--study": "The study name",
        "--release": "The study version"
    }

    def read_cli(self) -> dict:
        """
        Reads the various cli arguments (e.g. --file) and the corresponding value (e.g. ./file/to/path)
        from sys.argv.\n
        If the argument `--help` or `--version` or their short version (`-h` and `-v` respectively) is provided,
        the corresponding method is called, thus returning nothing.
        Other valid arguments can be found in CLIReader.valid_arguments
        The arguments can be unordered. Arguments that occur twice or more are overwritten,
        so only the last appearance will be present in the return value.
        Arguments are then validated using :py:function:validate_arguments().

        :return: a dictionary of all the arguments and their respective values
        :raises IndexError: If the value for the argument is omitted or a file and study information are provided.
        """
        args = {"--db": "mysql"}  # a dictionary of the cli arguments
        for arg in sys.argv:
            # skips the first two arguments ('python3' & module_name)
            if not arg.startswith("--"):
                continue

            if arg.lower() == "-h" or arg.lower() == "--help":
                self.print_help()
            elif arg.lower() == "-v" or arg.lower() == "--version":
                self.print_version()
            elif arg.lower() == "-d" or arg.lower() == "--debug":
                args["--debug"] = True
            else:
                item = arg.split('=', 1)
                args[item[0].lower()] = item[1]

        if not self.validate_arguments(args):
            self.print_help()

        return args

    def validate_arguments(self, args: dict) -> bool:
        """
        Checks if the user inputs into the CLI, meets the requirements.
        For further information refer to section "Usage".

        :param args: the dict of CLI arguments
        :return: True, if valid, False otherwise
        """
        # any invalid argument given?
        for arg in args.keys():
            if arg not in self.valid_arguments:
                return False

        # file and study given
        if args.get("--file") is not None and \
                args.get("--study") is not None:
            print("Es darf nur --file oder --study angegeben werden")
            return False

        # neither file nor study given
        if args.get("--file") is None and args.get("--study") is None:
            print("Es muss mindestens eines der Argument --file oder --study angegeben werden.")
            return False

        # study given but not version or version given but not study
        if (args.get("--study") is None and args.get("--release") is not None) or \
                (args.get("--study") is not None and args.get("--release") is None):
            print("Studienname definiert aber ohne Version oder Studienname nicht gegeben aber die Version.")
            return False

        # db has invalid value
        if args.get("--db") not in ("neo4j", "mysql"):
            print("Wert für --db darf nur entweder 'mysql' oder 'neo4j' sein.")
            return False

        return True

    def print_help(self) -> None:
        """
        Prints the usage of the cli arguments.\n
        Terminates the execution afterwards.
        """
        print(f"Usage: python3 PDG.py ARGUMENT (VALUE) [ARGUMENT (VALUE) ...]")
        for i in self.valid_arguments:
            print(i + "\t\t" + self.valid_arguments[i])
        exit(1)

    def print_version(self):
        """
        Prints the version of the PDG. Terminates program afterwards.
        """
        print("v1.0")  # todo: replace with actual version
        exit(1)


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
