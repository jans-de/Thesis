from contextlib import contextmanager
from os import system

from mysql.connector.cursor import MySQLCursorDict

import MySQLTransactions as Actions
from MySQLConnectionHandler import MySQLConnectionHandler
from nodes.Form import Form
from nodes.LookupClass import LookupClass
from nodes.Node import Node
from nodes.Study import Study
from nodes.Wrap import Wrap
from relations.Relation import Relation
from nodes import NodeUtils as nutils


class MySQLTransactionHandler:
    """
    This is the executing part of the MySQL DB_NAME part (thus the form_name).
    """
    DB_NAME = ""
    system_id = -1
    study_id = -1
    form_ids = {}
    connection_pool = None

    def __init__(self, database="mysql"):
        self.DB_NAME = database
        self.connection_handler = MySQLConnectionHandler(database)
        self.connection_pool = self.connection_handler.open_connection()

    @contextmanager
    def get_connection(self):
        """
        Returns a connection from the connection pool. If the connection pool is None,
        a new connection pool is created and cached accordingly.
        Otherwise, a new connection is obtained from the connection pool and returned.
        Finally, the connection is returned to the connection pool.

        This function is implement as a custom context manager, so that it can be used like
        `with get_connection() as conn:
            # code goes here
        `
        This approach eliminates repetitive boilerplate code like:
        ` try:
            connection = self.get_connection()
            # do stuff
        finally:
            connection.close()
        `
        :return:
        """
        # __enter__()
        # in case the connection pool was not created properly
        if self.connection_pool is None:
            connection_handler = MySQLConnectionHandler(self.DB_NAME)
            self.connection_pool = connection_handler.open_connection()

        # code which is executed with `with`
        conn = self.connection_pool.get_connection()
        yield conn

        # __exit__()
        conn.close()

    @staticmethod
    def prepare_db():
        with open("./conf/mysql.auth", 'r', closefd=True) as file:
            lines = file.read().splitlines()
            user = lines[0]
            pwd = lines[1]
        file = "./../mysql-files/create_structure.sql"
        command = """mysql -u %s -p"%s" < %s""" % (user, pwd, file)
        system(command)

    def insert_study(self, study: Study):
        """
        Inserts the given study.

        :param study: the Study object to be inserted
        """
        query, values = Actions.insert_study(study)
        c = self.run_query(query, values)
        self.study_id = c.lastrowid

    def set_study(self, study: Study):
        """
        This function sets the class variable study_id.
        This function is used in GeneratorController.main() .

        :param study: the Study to be cached
        """
        query, values = Actions.get_study_id_by_node(study)
        c = self.run_query(query, values)
        # study name is unique, so we can safely access the first (and only (hopefully...)) result of this query
        self.study_id = c.fetchone()['id']

    def insert_forms(self, form_list: list[Form]):
        """
        Inserts the forms of the study.
        The forms are inserted in-order of the list, which, when the ADL-file is read correctly,
        obsoletes the need of creating connection to the previous form.

        The id's of the forms in the MySQL are cached after insertion and stored in the class
        variable `form_ids`.

        :param form_list: the list of form nodes
        """
        for i, form in enumerate(form_list):
            query, values = Actions.insert_form(self.study_id, form)
            cursor = self.run_query(query, values)
            self.form_ids[form.get_form_name()] = cursor.lastrowid

    def get_forms(self) -> list[Form]:
        query, values = Actions.get_forms(self.study_id)
        c = self.run_query(query, values)

        form_list = []
        for i in c.fetchall():
            form_list.append(Form(i.get("name"), i["type"]))
        return form_list

    def insert_lookups(self, lookups: list[LookupClass]):
        for idx, lu in enumerate(lookups):
            self.insert_lookup_class(idx, lu)

    def insert_lookup_class(self, idx, lookup_class: LookupClass):
        name = lookup_class.get_name()
        for entry, value in lookup_class.get_definitions().items():
            query, values = Actions.insert_lookup_class(idx, name, entry, value)
            self.run_query(query, values)

    def get_lookup_by_name(self, lookup_class_name: str) -> LookupClass:
        query, values = Actions.get_luc_by_name(lookup_class_name)
        c = self.run_query(query, values)

        tmp = {}
        for row in c.fetchall():
            tmp["name"] = row.get("name", "")
            tmp[row.get("entry")] = row.get("value")
        return LookupClass(tmp)

    def get_lookup_id_by_name(self, lookup_class_name: str) -> int:
        query, values = Actions.get_luc_by_name(lookup_class_name)
        c = self.run_query(query, values)

        return c.fetchone()["classId"]

    def insert_single_field(self, node: Node) -> int:
        form_name = node.get_id().split("/")[-2]
        query, values = Actions.get_form_by_name(self.study_id, form_name)
        c = self.run_query(query, values)
        form_id = c.fetchone()["id"]

        if node.get_sbuild_type() == "Radio" or node.get_sbuild_type() == "Lookup":
            # we can ignore the warning 'Unresolved attribute reference' because the if-clause ensures,
            # that the node has a lookup class in this block
            query, values = Actions.get_luc_by_name(node.get_lookup_class())
            c = self.run_query(query, values)
            luc_id = c.fetchone()["classId"]
            query, values = Actions.insert_field_with_luc(form_id, luc_id, node)
        else:
            query, values = Actions.insert_field(form_id, node)

        c = self.run_query(query, values)
        field_id = c.lastrowid

        self.insert_field_attributes(field_id, node)

        return field_id

    def insert_field(self, parent, rel, node):
        # get form id from cache
        form_id = self.form_ids.get(parent.get_form_name(), -1)

        # insert field
        if node.get_sbuild_type() == "Radio" or node.get_sbuild_type() == "Lookup":
            luc_id = self.get_lookup_id_by_name(node.get_lookup())
            query, values = Actions.insert_field_with_luc(form_id, luc_id, node)
        else:
            query, values = Actions.insert_field(form_id, node)
        c = self.run_query(query, values)
        field_id = c.lastrowid

        # insert remaining attributes
        self.insert_field_attributes(field_id, node)

    def insert_child(self, parent: Node, rel: Relation, child: Node):
        from_id = self.get_field_id(parent)
        to_id = self.insert_single_field(child)
        self.insert_relation(from_id, to_id, rel)

    def get_field_id(self, node: Node):
        study = node.get_id().split("/")[2]
        version = node.get_id().split("/")[3]
        form = node.get_id().split("/")[-2]

        query, values = Actions.get_field(study, version, form, node)
        c = self.run_query(query, values)
        return c.fetchone()["fieldId"]

    def get_children_of_form(self, form: Form) -> list[Node]:
        return self.get_nodes(form)

    def get_children_of_node(self, node: Node) -> list[Node]:
        return self.get_nodes(node)

    def get_nodes(self, node: Form | Node) -> list[Node]:
        if type(node) is Form:
            query, values = Actions.get_children_of_form(self.study_id, node)
        elif type(node) in Wrap.__subclasses__():
            query, values = Actions.get_children_of_field(node.get_id())
        else:
            return []

        c = self.run_query(query, values)

        children = []
        for row in c.fetchall():
            attributes = self.get_field_attributes(row["fieldId"])
            if len(attributes) == 0:
                continue
            row.update(attributes)
            row.update({"id": row.get("identifier", "")})
            children.append(nutils.create_node(row))

        return children

    def insert_field_attributes(self, field_id, node: Node):
        """
        Inserts all attributes of this node, which are not a column in the relation `Fields`.

        :param field_id: the field to which these attributes belong
        :param node: the Widget or Wrap whose attributes should be inserted
        """
        # these attributes don't belong into FieldAttributes
        excluded_attributes = ['id', 'name', 'category', 'mandatory', 'fieldId', 'buildType', 'sbuildType',
                               'children', 'lookupClass', 'triggeredElements']

        for attribute, value in node.get_all_attributes().items():
            if attribute not in excluded_attributes and type(value) is not dict:
                query, values = Actions.insert_field_attributes(field_id, nutils.to_camel_case(attribute), value)
                self.run_query(query, values)

    def get_field_attributes(self, field_id: int) -> dict:
        """
        This query retrieves all rows of FieldAttributes of the given field_id
        as a dictionary of unknown length.

        :param field_id: the id of the field
        :return: a dictionary represantation of all attributes of this field.
        """
        query, values = Actions.get_field_attributes_by_id(field_id)
        c = self.run_query(query, values)
        attributes = {}
        for row in c.fetchall():
            attributes[row.get("attribute", "")] = row.get("value", "")
        return attributes

    def insert_relation(self, from_id: int, to_id: int, rel: Relation):
        """
        Inserts the given Relation.

        :param from_id: the id of the source field
        :param to_id: the id of the destination field
        :param rel: the relation to be inserted
        """
        query, values = Actions.insert_relation(from_id, to_id, rel)
        self.run_query(query, values)

    def run_query(self, query, values, buffered=True) -> MySQLCursorDict:
        with self.get_connection() as conn:
            cursor = conn.cursor(buffered=buffered, dictionary=True)
            query = query.strip()
            cursor.execute(query, values)
            # commit the changes
            conn.commit()
        return cursor


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
