from neo4j import GraphDatabase, Driver


class Neo4jConnectionHandler:
    """
    Handles the connection to the Neo4j graph database.
    """

    def __init__(self):
        """
        Initializes a new instance of the Neo4jConnectionHandler class.
        Sets the property `driver` by calling `open_connection()`
        """
        self.driver = self.open_connection()

    def get_driver(self) -> Driver:
        """
        Getter for driver.
        self.driver is first called in the constructor of this class.
        If the driver is None, opens a new connection.

        :return: self.driver
        """
        if self.driver is None:
            self.driver = self.open_connection()
        return self.driver

    @staticmethod
    def open_connection() -> Driver:
        """
        Initializes the connection to the neo4j graph DB_NAME using the credentials in *./conf/neo4j.auth* .\n

        :returns: a new Driver object

        :raises FileNotFoundError: if the credentials could not be opened.
        :raises ConnectionError: if the connection attempt times out after 20 seconds
        """

        # 'with' prevents memory leaks, raises exception automatically
        with open("./conf/neo4j.auth", 'r', closefd=True) as file:
            lines = file.read().splitlines()
            user = lines[0]
            pwd = lines[1]

        # first parameter is the default uri
        # second the authorisation params
        with GraphDatabase.driver("bolt://localhost:7687", auth=(user, pwd)) as driver:
            try:
                driver.verify_connectivity()
            except Exception as e:
                driver.close()
                print("Could not connect to Neo4j database.")
                print(e.__traceback__)
                raise e

        return driver


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
