import timeit

from PDG import PDG


def main():
    study_name = "study"
    version = "1."

    names = []
    for i in range(10):
        for v in range(5):
            names.append({
                "--db": "neo4j",
                "--study": study_name + str(i),
                "--release": version + str(v)
            })

    for idx, study in enumerate(names):
        start = timeit.default_timer()
        pdg = PDG()
        pdg.main(study)

        # stop timer and return the difference aka. the duration of the execution
        duration = timeit.default_timer() - start
        with open("times_neo2.txt", "a") as f:
            print("finished nr ", str(idx))
            f.write(str(idx) + ", " + str(duration)[0:6])


if __name__ == "__main__":
    main()
