import os
import timeit

from PDG import PDG
NR_OF_STUDIES = 50
NR_OF_VERSIONS = 10



def main():
    study_names = []
    for i in range(NR_OF_STUDIES):
        study_names.append("study" + str(i))

    versions = []
    for i in range(NR_OF_VERSIONS):
        versions.append("1." + str(i))

    # create a composite name of sponsor_study, study and version
    names = []
    for s in study_names:
        for v in versions:
            names.append("sponsor_" + s + "/" + s + "/" + v)

    # iterate through composite names
    for it, name in enumerate(names):
        # read input file
        with open("./adl_files/demo.json", "r") as file:
            content = file.read()

        content = content.replace("sponsor/study/1.0", name)
        content = content.replace('"name": "study"', f'"name": "{name.split("/")[1]}"', 1)
        content = content.replace('"system": "sponsor"', f'"system": "{name.split("/")[0]}"', 1)
        content = content.replace('"version": "1.0"', f'"version": "{name.split("/")[2]}"', 1)
        filename = "./benchmark/" + name.replace("/", "_") + ".json"

        # create new adl-file with the replaced content
        with open(filename, "w", newline="") as file:
            file.write(content)

        start = timeit.default_timer()

        pdg = PDG()
        args = {
            "--db": "neo4j",
            "--file": filename,
            "study_nr": it
        }

        # set start point of execution in a platform-independent manner

        # pdg execution
        pdg.main(args)

        # stop timer and return the difference aka. the duration of the execution
        duration = timeit.default_timer() - start
        with open("../time.txt", "a") as timetable:
            nr = args.get("study_nr")
            print(f"nr {nr} finished")
            timetable.write(str(nr) + ", " + str(duration)[0:7] + "\n")

        os.remove(filename)


if __name__ == "__main__":
    main()
