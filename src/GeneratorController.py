from DataGenerator import DataGenerator
from GeneratorWriter import GeneratorWriter
from MySQLTransactionHandler import MySQLTransactionHandler
from Neo4jTransactionHandler import Neo4jTransactionHandler
from nodes.Form import Form
from nodes.LookupClass import LookupClass
from nodes.Node import Node
from nodes.Study import Study
from nodes.Widget import WidgetLookup, WidgetRadio


class GeneratorController:
    """
    This is the main class of the DataGeneration.
    The constructor initializes the controllers of the databases as well as the DataGenerator.

    This class handles the interaction between the databases, generates values and forwards the values to the
    GeneratorWriter class where the values are written into the CSV-file.

    Attributes:
        args (dict): the dictionary for CLI-arguments
        NR_OF_PATIENTS ()
    """
    args = {}
    NR_OF_PATIENTS = 5
    NR_OF_FORMS = 5

    study: Study = None
    data_generator = None

    forms: list[Form]  # cached list of form nodes

    # protected start timer of generation
    __start: float = 0

    def __init__(self, args: dict):
        self.args = args
        if args.get("--db") == "mysql":
            self.handler = MySQLTransactionHandler("pdg_dev")
        else:
            self.handler = Neo4jTransactionHandler("neo4j")
        self.data_generator = DataGenerator()

    def main(self):
        study = self.args.get("--study")
        version = self.args.get("--release")
        # set study if given, else abort generation
        if study is None or version is None:
            return
        else:
            # cache study
            self.study = Study("", study, "", version)
            self.handler.set_study(self.study)

        patient_data = self.generate_patient_data()
        fields = list(patient_data.keys())
        writer = GeneratorWriter(self.args)
        writer.prepare_data_sheet(fields)

        # generate n-1 patients, because one is already generated above
        for i in range(self.NR_OF_PATIENTS - 1):
            # I could optimize this loop by caching the list of nodes,
            # but since I want to challenge the databases, I implemented it the non-optimal way
            patient_data = self.generate_patient_data()
            patient_data.update(self.generate_dates())
            writer.write_row(patient_data)

    def generate_patient_data(self):
        """
        "Main" method for patient data generation.
        Retrieves all fields from the form, then calls :py:func:`iterate_fields`.
        Finally, a dictionary consisting of {<form_name.field_name_<iteration>>: value} is merged and returned.

        :return: a flattened list of dictionaries where key = fieldname and value = a value
        """
        forms = self.get_forms()
        patient_data = {}

        # generate data per form and appends them to patient_data
        for form in forms:
            if form.get_form_type() == "one":
                # get the immediate children of each form
                fields = self.get_children_of_form(form)
                data = self.iterate_fields(fields)
                data = {form.get_form_name() + "." + k: v for k, v in data.items()}
                patient_data.update(data)
            else:
                # generate the data n times
                for i in range(self.NR_OF_FORMS):
                    fields = self.get_children_of_form(form)
                    data = self.iterate_fields(fields, iteration=i)
                    data = {form.get_form_name() + "." + k + "_" + str(i): v for k, v in data.items()}
                    patient_data.update(data)

        return patient_data

    def iterate_fields(self, field_list: list[Node], **kwargs):
        """
        This function is supposed to iterate the tree and generate values for each field per form on the fly.
        It returns a list of dictionaries in the format: {fullname: value}
        The parameter field_list is included in the return value.
        aka. form.form_type == "many"

        :return: the updated field list
        """
        # create dict and fill with the "parent" fields
        patient_data = kwargs.get("patient_data", {})
        # generate values for every field in field_list
        patient_data.update(self.generate_values(field_list, iteration=kwargs.get("iteration")))

        # get children of every field in field_list using DFS
        for f in field_list:
            children = self.get_children_of_node(f)
            # filter out None elements
            children = [c for c in children if c is not None]

            # todo implement
            # if type(f) is WrapSlider:
            #     # based on this value, get the list of children of f (the parent)
            #     for c in children:
            #         if c.get_id == f.get_trigger_element():
            #             trigger_elem = c
            #             break
            #     # generate a value for the trigger element
            #     val = self.generate_values([trigger_elem])
            #     triggered_elems = []
            #
            # if type(f) is WrapOr:
            #     # get a random element from children
            #     children = [random.choice(children)]

            # recursively call iterate_fields() on every child
            self.iterate_fields(children, patient_data=patient_data, iteration=kwargs.get("iteration"))

        return patient_data

    def generate_values(self, field_list: list[Node], **kwargs) -> dict:
        """
        Calls :function:`get_value()` on a list of Nodes.

        :param field_list: a list of Nodes of which the values should be generated
        :return: a dictionary containing the field_identifier as key and a corresponding value
        """
        values = {}
        lookup = None

        if self.data_generator is None:
            self.data_generator = DataGenerator()

        for f in field_list:
            if type(f) is WidgetRadio or type(f) is WidgetLookup:
                # warning can be ignored, because f always has a lookup
                lookup = self.get_lookup(f)
            val = self.data_generator.generate_value(f, iteration=kwargs.get("iteration"), lookup=lookup)
            if val is not None:
                values[f.get_name()] = val
        return values

    def get_forms(self):
        """
        Returns all forms of the study.
        From which study the forms are retrieved is determined by the class variable `study`.

        :return: a list of Form objects. The list is FIFO-ordered.
        """
        return self.handler.get_forms()

    def generate_dates(self) -> dict[str, str]:
        """
        Wrapper function for DataGenerator.generate_dates().

        :return: a dictionary of names and dates
        """
        return self.data_generator.generate_dates()

    def get_children_of_form(self, form: Form):
        children = self.handler.get_children_of_form(form)
        children = [c for c in children if c is not None]
        return children

    def get_children_of_node(self, node: Node):
        """
        This function is BFS by design
        todo refactor to DFS, e.g. order by the new field_id

        :param node: the parent of which to get the children
        :return: a tuple of the parent and a list of child nodes
        """
        return self.handler.get_children_of_node(node)

    def get_lookup(self, f: WidgetRadio | WidgetLookup) -> LookupClass:
        return self.handler.get_lookup_by_name(f.get_lookup_class())


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
