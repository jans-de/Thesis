import mysql.connector
from mysql.connector import errorcode


class MySQLConnectionHandler:

    def __init__(self, database):
        """
        Initialises a new MySQLConnectionHandler object.
        During initialisation the class variable DB_NAME is set.

        :param database: used to initialise the class variable `DB_NAME`
        """
        self.DB_NAME = database

    def open_connection(self):
        """
        Establishes a connection to the MySQL DB_NAME at 127.0.0.1:3306 (standard configuration).
        Authentication values are stored in conf/mysql.auth and read during runtime.

        The corresponding DB_NAME form_name is taken from class variable DB_NAME.

        :raises mysql.connector.errorcode.ER_BAD_DB_ERROR: if the db form_name can't be found
        :raises mysql.connector.errorcode.ER_ACCESS_DENIED_ERROR: if the auth variables are faulty
        :raises mysql.connector.Error: on unspecified errors
        """

        with open("./conf/mysql.auth", 'r', closefd=True) as file:
            lines = file.read().splitlines()
            user = lines[0]
            pwd = lines[1]

        config = {
            'user': user,
            'password': pwd,
            'host': '127.0.0.1',
            'port': 3306
        }
        if self.DB_NAME != "":
            config.update({'database': self.DB_NAME})

        # sample connect code
        # from https://dev.mysql.com/doc/connector-python/en/connector-python-example-connecting.html
        try:
            # explicitly create a MySQLConnectionPool
            pool_name = self.DB_NAME + "_pool"
            pool = mysql.connector.pooling.MySQLConnectionPool(pool_name=pool_name, pool_size=3,
                                                               pool_reset_session=False, **config)
            return pool
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                print(f"Error: DB_NAME {self.DB_NAME} does not exist.")
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Error: user form_name or password incorrect. Access denied.")
            else:
                print("Error code: ", err.errno)
                print("Error message: ", err.msg)

            raise err


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
