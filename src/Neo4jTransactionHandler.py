from contextlib import contextmanager

from neo4j import Session

import Neo4jTransactions as Actions
from Neo4jConnectionHandler import Neo4jConnectionHandler
from nodes import Study, Node, NodeUtils as nutils
from nodes.LookupClass import LookupClass
from nodes.Form import Form
from relations import Relation


# noinspection PyTypeChecker
class Neo4jTransactionHandler:
    DB_NAME = ""

    # cached variables
    study: Study = None
    conn_handler = None
    field_id = 0

    def __init__(self, database="neo4j"):
        self.DB_NAME = database
        self.conn_handler = Neo4jConnectionHandler()

    def set_study(self, study: Study):
        self.study = study

    @contextmanager
    def get_session(self) -> Session:
        """
        Opens and returns a session to the Neo4j database.

        During __enter__(), it checks if a 'Neo4jConnectionHandler' object is initialised (not None).
        In this case a new 'self.conn_handler' is set.

        Creates a session based on 'self.conn_handler.driver' to the database 'self.DB_NAME'.

        Closes the session in __exit__()
        :return: a new session
        """
        # __enter__()
        if self.conn_handler is None:
            self.conn_handler = Neo4jConnectionHandler()

        sess = self.conn_handler.get_driver().session(database=self.DB_NAME)
        yield sess

        # __exit__()
        sess.close()

    def prepare_db(self):
        with self.get_session() as session:
            session.execute_write(Actions.teardown_db)

    def insert_study(self, study: Study) -> bool:
        """
        Insert
        :param study:
        :return:
        """
        with self.get_session() as session:
            res = session.execute_write(Actions.insert_study_node, study)

        # cache the study node
        self.study = study
        return list(res) != 0

    def insert_forms(self, form_list: list[Form]) -> list:
        """
        This method requires a function call of `insert_study` prior, in order to work!
        Otherwise, the forms are not attached to the study node.

        :param form_list:
        """
        prev = None

        with self.get_session() as session:
            # insert every form
            for form in form_list:
                form.study_version = self.study.get_study_name() + self.study.get_study_version()
                res = session.execute_write(Actions.insert_form, form, self.study, prev)
                prev = form

        return list(res)

    def get_forms(self) -> list[Form]:
        """
        Executes the Neo4j query :py:func:`Neo4jTransactions.get_form_by_name`.
        This query takes the name and version of the study and returns a list of forms.
        The forms are cast to FormNodes and finally returned.

        :return: a list of Form objects. The list is ordered according to the FIFO principle.
        """
        study = self.study
        with self.get_session() as session:
            res = session.execute_read(Actions.get_forms_of_study, study.get_study_name(), study.get_study_version())

        form_list = []
        for item in res:
            form_dict = item.data()
            name = form_dict["f"]["formName"]
            rel_type = form_dict["f"]["formType"]
            form_list.append(Form(name, rel_type))
        return form_list

    def insert_lookups(self, lookups: list[LookupClass]):
        with self.get_session() as session:
            for lookup in lookups:
                session.execute_write(Actions.insert_lookup, self.study, lookup)

    def get_lookup_by_name(self, lookup_name: str):
        with self.get_session() as session:
            res = session.execute_read(Actions.get_lookupclass, self.study, lookup_name)
        return LookupClass(res["l"])

    def insert_field(self, parent: Node, rel: Relation, child: Node):
        """
        Appends the child node to the parent node using the specified relation.
        Calls self.assign_lookupclass() after insert.

        :param parent: the parent node
        :param rel: the relation between the nodes
        :param child: the actual node to be inserted
        :return:
        """
        child.field_id = self.field_id
        self.field_id += 1
        with self.get_session() as session:
            session.execute_write(Actions.insert_field, self.study, parent, rel, child)

        self.assign_lookupclass(child)

    def insert_child(self, parent: Node, rel: Relation, child: Node):
        """
        Appends the child node to the parent node with the specified relation in between.
        Calls self.assign_lookupclass() after insert.

        This query differs from self.insert_field() in todo: why?

        :param parent: the parent node
        :param rel: the relation between the nodes
        :param child: the child node to be appended
        """
        child.field_id = self.field_id
        self.field_id += 1
        with self.get_session() as session:
            res = session.execute_write(Actions.insert_child, self.study, parent, rel, child)
        self.assign_lookupclass(child)

    def assign_lookupclass(self, child: Node):
        """
        Creates a relation to the corresponding LookupClass node, if the child has the attribute 'lookup_class'.

        :param child: the child element
        """
        if len(child.get_attribute("lookup_class")) != 0:
            with self.get_session() as session:
                session.execute_write(Actions.assign_lookupclass, self.study, child, child.get_lookup_class())

    def get_children_of_form(self, form: Form) -> list[Node]:
        """
        This function calls Actions.get_children_of_form, thus getting the immediate children of the form.
        The child nodes are cast to Nodes and returned.
        Immediate meaning to a depth of 1.

        :param form: the form of the desired forms
        :return: the list of Node objects of this form
        """
        with self.get_session() as session:
            res = session.execute_read(Actions.get_children_of_form, self.study, form)

        field_list = []
        for item in res:
            field_dict = item.data()
            node = nutils.create_node(field_dict["children"])
            field_list.append(node)
        return field_list

    def get_children_of_node(self, node: Node) -> list[Node]:
        with self.get_session() as session:
            res = session.execute_read(Actions.get_children_of_node, self.study, node.get_id())

        # node with id = identifier, has no children
        if len(res) == 0:
            return []

        field_list = []
        for item in res:
            node = nutils.create_node(item.data()["children"])
            field_list.append(node)

        return field_list


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
