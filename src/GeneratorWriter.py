import csv
from csv import DictWriter
from os import system


class GeneratorWriter:

    args = {}
    writer = None

    def __init__(self, args: dict):
        self.args = args

    def prepare_data_sheet(self, fieldnames: list):
        # create file in ./data/, filename like <study>_<version>_data.csv
        filepath = "./data/" + self.args.get("--study", "study") + "_" + self.args.get("--release", "") + "_data.csv"
        self.args["filepath"] = filepath
        command = """touch %s""" % filepath
        system(command)

        # insert header row
        with open(filepath, "w", newline="") as csvfile:
            csv_writer = DictWriter(csvfile, fieldnames=fieldnames, dialect="unix", quoting=csv.QUOTE_MINIMAL)
            csv_writer.writeheader()

    def write_row(self, values: dict):
        """
        Writes the whole row into the output file.

        :return:
        """
        # the flag 'a' tells open() to append a new row instead of starting from the first one
        with open(self.args["filepath"], "a", newline="") as file:
            writer = csv.DictWriter(file, quoting=csv.QUOTE_MINIMAL, fieldnames=list(values.keys()))
            writer.writerow(values)
