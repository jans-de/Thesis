class Relation:
    """
    Data class for a relation between a Wrap (parent/source) and a Widget(child/destination).
    In the graph database the relation can also be between a form (parent) and a Wrap or Widget (child).
    The relation does not specify the source, because the relations are stored in the parent element.

    Attributes:
        relation_type (str): the type of relation
        dest (str): the destination of the relation. This var should only be set, if relation type is TRIGGERS,
            to differentiate between the TriggerElements.
        on_value (str): the trigger value of this relationship
    """
    relation_type: str
    dest: str
    on_value: str

    def __init__(self, relation_type, dest: str = None, on_value: str = ""):
        self.relation_type = relation_type
        self.dest = dest
        self.on_value = on_value

    def get_relation_type(self) -> str:
        """
        Getter for attribute relation_type.
        :return: self.relation_type
        """
        return self.relation_type

    def get_dest(self) -> str | None:
        """
        Getter for attribute dest.
        :return: self.dest or, if not set, None
        """
        return self.dest

    def get_on_value(self) -> str | None:
        """
        Getter for attribute on_value.
        :return: self.on_value or, if not set, None
        """
        return self.on_value
