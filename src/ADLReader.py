import json
import nodes.NodeUtils as nutils
from typing import Any

from MySQLTransactionHandler import MySQLTransactionHandler
from Neo4jTransactionHandler import Neo4jTransactionHandler
from nodes.Form import Form
from nodes.LookupClass import LookupClass
from nodes.Study import Study
from nodes.Wrap import WrapSlider
from relations.Relation import Relation


class ADLReader:
    """
    todo: write doc for class ADLReader
    """

    BASE_NAME = "alcedis/"

    def __init__(self, args: dict):
        """
        Constructor for class ADLReader.
        Initializes the *TransactionHandler based on the entry "--db" from the parameter args.
        :param args: the CLI arguments passed down from PGD.py
        """
        if args.get("--db") == "mysql":
            self.handler = MySQLTransactionHandler("pdg_dev")
        else:
            self.handler = Neo4jTransactionHandler("neo4j")

    def read_adl_from_file(self, args):
        """
        This file reads the ADL file and inserts the elements into the database.
        Truncates the database in debug mode.

        :param args: the CLI arguments passed down from PGD.py
        """

        # open ADL file
        try:
            file = open(args.get("--file"), 'r')
        except KeyError:
            print("File path not found. Exiting...")
            exit(-1)
        except FileNotFoundError:
            print("Couldn't find ADL file. Exiting...")
            exit(-1)
        except OSError:
            print("Unknown exception while opening ADL file. Exiting...")
            exit(-1)

        data = json.load(file)
        ADLReader.BASE_NAME = data["id"]

        # truncates databases to avoid issues during insert
        if args.get("--debug"):
            print("Debug mode enabled. Truncating databases.")
            # delete and recreate db
            self.handler.prepare_db()

        # extract and insert study info
        study = self.extract_study_info(data)
        self.handler.insert_study(study)
        # print("Study successfully inserted")

        # extract and insert forms
        form_list = self.extract_forms(data)
        self.handler.insert_forms(form_list)
        # print("Forms successfully inserted")

        form_data = data["resources"]
        # insert lookups
        lookups = [form_data[i] for i in form_data if i.startswith(ADLReader.BASE_NAME + "lookup")]
        lookup_list = [LookupClass(lookup) for lookup in lookups]
        self.handler.insert_lookups(lookup_list)
        # print("Lookups successfully inserted")

        for form in form_data.values():
            # get current form
            parent = self.get_form_node_from_list(form.get("name", ""), form_list)
            if parent is not None:
                self.insert_fields(form["children"], parent, 0)

        # print("Fields successfully inserted")

    @staticmethod
    def extract_study_info(data) -> Study:
        s = Study(data["system"], data["name"], data["title"], data["version"])
        return s

    @staticmethod
    def extract_forms(data: Any) -> list[Form]:
        """
        Reads all patient forms, i.e. all forms which are filled out per patient.
        This excludes every form which is filled out per organisation (aka. non-patient forms).
        The returned list is arranged according to the order in which the forms are to be completed by
        user of the eCRF.

        :param data:
        :return: the list of patient forms
        """
        form_list = []
        patient_forms = data["navigation"]["main"]["default"][1]["children"]

        for form in patient_forms:
            # extract the form name and save the name in form_list
            curr_form_name = str(form["id"].replace(ADLReader.BASE_NAME + "navigation/main/default/patient/", ""))

            # skip the form 'content'
            if curr_form_name == "content":
                continue

            # determines the type of relationship
            # if '*/index/<form_name>/*' does not exist, that means that this form does not have a list
            rel_type = data["resources"].get(ADLReader.BASE_NAME + "index/" + curr_form_name)
            if rel_type is None:
                rel_type = "one"
            else:
                rel_type = "many"

            curr_form = Form(curr_form_name, rel_type)
            form_list.append(curr_form)

        return form_list

    def insert_fields(self, form_data: dict, parent, field_id=0):
        """
        This function inserts the fields of the parent. The parent is either a form or another field, which has
        children. form_data.values is iterated and per value of this dict, a new (child) node and the
        appropriate relation is created. If the created node is None, e.g. this Node is not supported,
        then the iteration continues to the next child of the parent.
        Otherwise, if the parent is a form, the insert the nodes to the form node in the db.
        If not, then append a child node to the parent node.

        :param form_data: the ADL file as a dictionary
        :param parent: the parent node. Either a form or a Widget/Wrap
        :param field_id: the id of the field
        """
        for field in form_data.values():
            node = nutils.create_node(field)
            rel = Relation("CONTAINS")

            if node is None or parent is None:
                continue

            node.field_id = field_id

            # if the parent is a form, append children to this form
            if type(parent) is Form:
                self.handler.insert_field(parent, rel, node)

            # else append children to the parent node
            else:
                # if the parent is a Slider
                if type(parent) == WrapSlider:
                    # and the current child is not the trigger element
                    if node.get_id() != parent.get_trigger_element():
                        rel = parent.get_triggered_relation_by_field_name(node.get_id())
                    # or create an 'IS_TRIGGER_ELEM' relationship
                    else:
                        rel = Relation("IS_TRIGGER_ELEM")
                self.handler.insert_child(parent, rel, node)

            if node.has_children():
                self.insert_fields(field["children"], node)

    @staticmethod
    def get_form_node_from_list(name, form_list: list[Form]):
        """
        Returns the form node of the given form name from the form list.

        :param name: the form you're looking for
        :param form_list: the list of all forms
        :return: the form node ob
        """
        if name == "":
            return None
        for i in form_list:
            if name == i.get_form_name():
                return i


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
