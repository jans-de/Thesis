from neo4j import Session

from nodes.LookupClass import LookupClass
from nodes.NodeUtils import attributes_to_string, remove_attribute

from nodes.Form import Form
from nodes.Node import Node
from nodes.Study import Study
from relations.Relation import Relation


# todo order the functions like in MySQLTransactions
# todo create index on (widget|wrap).id
# todo add error handling
def teardown_db(ses: Session):
    """
    Deletes all values from the database. This should only happen in debug mode.

    :param ses: the Neo4j session
    """
    query = "MATCH (n) DETACH DELETE (n)"
    ses.run(query)


def insert_study_node(ses: Session, study: Study) -> list:
    """
    Insertion method for the Study.

    :param ses: the Neo4j session
    :param study: the Study object to be inserted
    :return: the Result object cast to list
    """
    query = f"MERGE (s:Study {attributes_to_string(study)})"
    values = {}
    values.update(study.__dict__)
    res = ses.run(query, values)
    return list(res)


def insert_form(ses: Session, form: Form, study: Study, prev_form: Form = None) -> list:
    """
    Inserts the given form and connects it to the corresponding study.
    If prev_form is not None, the form is also connected to this with a 'REQUIRES' relationship.

    :param ses:
    :param form:
    :param study:
    :param prev_form:
    :return:
    """
    if prev_form is None:
        query = "MATCH (s:Study {studyName: $study, studyVersion: $version}) " \
                f"MERGE (f:Form {attributes_to_string(form)})<-[:CONTAINS]-(s)"
        values = {
            "study": study.get_study_name(),
            "version": study.get_study_version()
        }
        values.update(form.__dict__)
    else:
        query = "MATCH (s:Study {studyName: $study, studyVersion: $version})-[*1]->" \
                "(f_pre:Form {formName: $name_pre})" \
                f"MERGE (f_pos:Form {attributes_to_string(form)}) " \
                "MERGE (f_pos)-[:REQUIRES]->(f_pre) " \
                "MERGE (s)-[:CONTAINS]->(f_pos)"
        values = {
            "study": study.get_study_name(),
            "version": study.get_study_version(),
            "name_pre": prev_form.get_form_name()
        }
        values.update(form.__dict__)
    res = ses.run(query, values)
    return list(res)


def get_forms_of_study(ses: Session, study_name: str, version: str) -> list:
    """
    Get the study and all forms from this study. The resulted is FIFO-ordered (First-in First-out).

    :param ses: the Neo4j session
    :param study_name: the name of the study
    :param version: the version of the study
    :return: a list consisting of a sole Neo4j.Result object
    """
    query = "MATCH (s:Study {studyName: $study_name, studyVersion: $version})-[:CONTAINS]->(f:Form) " \
            "RETURN f ORDER BY elementId(f) ASC"
    values = {
        "study_name": study_name,
        "version": version
    }
    res = ses.run(query, values)
    return list(res)


def insert_lookup(ses: Session, study: Study, lookup: LookupClass):
    query = "MATCH (s:Study {studyName: $study_name, studyVersion: $study_version}) " \
            f"MERGE (s)-[:HAS_LOOKUP]->(l:LookupClass {attributes_to_string(lookup)})"
    values = {
        "study_name": study.get_study_name(),
        "study_version": study.get_study_version(),
        "name": lookup.get_name()
    }
    values.update(lookup.get_definitions())
    res = ses.run(query, values)
    return list(res)


def assign_lookupclass(ses: Session, study: Study, node: Node, lookup_class: str):
    query = "MATCH path = shortestPath((s:Study)-[*1..25]->(n:Widget)) " \
            "WHERE s.studyName = $study_name AND studyVersion = $study_version AND n.id = $node_id" \
            "MATCH (s)-[:HAS_LOOKUP]->(l:LookupClass {name: $lookup_class}) " \
            "MERGE (n)-[:ASSIGNED_TO]->(l)"
    values = {
        "study_name": study.get_study_name(),
        "study_version": study.get_study_version(),
        "node_id": node.get_id(),
        "lookup_class": lookup_class
    }
    ses.run(query, values)


def get_lookupclass(ses: Session, study: Study, lookup_name: str) -> dict:
    """
    This Cypher query retrieves the LookupClass by LookupClass name.
    The query matches the Study node based on the provided studyName and studyVersion.
    Then, it traverses the relationship HAS_LOOKUP to reach the
    LookupClass node with the provided name. Finally, it returns the matched LookupClass node.

    We can assume that this query only yields one result, because LookupClass name is unique per Study.

    :param ses: a Neo4j session
    :param study: the study
    :param lookup_name: the name of the LookupClass
    :return: The LookupClass as a dictionary
    """
    query = "MATCH (s:Study {studyName: $study_name, studyVersion: $study_version})-[:HAS_LOOKUP*1]" \
            "-(l:LookupClass {name: $lookup_name}) return l"
    values = {
        "study_name": study.get_study_name(),
        "study_version": study.get_study_version(),
        "lookup_name": lookup_name
    }
    res = ses.run(query, values)
    return res.data()[0]


def insert_field(ses: Session, study: Study, parent: Form, rel: Relation, child: Node):
    """
    todo: maybe merge with insert_child() and rename to insert_node()
    Inserts the child to the form of the study and connects the child to the given form by rel.
    The attributes 'triggered_elements' and 'children' are omitted before Insert.
    These 2 attributes are lists, which causes exceptions in the database. I could also serialize them,
    but they're not important anyway.

    :param ses: a Neo4jSession
    :param study: the study which the field belongs to
    :param parent: the form which the field belongs to
    :param rel: the type of relationship between the parent and the child
    :param child: the node to be inserted
    :return: the result of the query as a list
    """
    query = "MATCH (s:Study {studyName: $study_name, studyVersion: $study_version})" \
            "-[:CONTAINS]->(f:Form {formName: $form_name}) " \
            f"MERGE (f)-[:{rel.get_relation_type()} {attributes_to_string(rel)}]->" \
            f"(w:{child.get_category()} {attributes_to_string(child)})"
    values = {
        "study_name": study.get_study_name(),
        "study_version": study.get_study_version(),
        "form_name": parent.get_form_name(),
    }
    values.update(child.__dict__)
    values.update(rel.__dict__)
    values = remove_attribute(values, "triggered_elements", "children", "dest")

    res = ses.run(query, values)
    return list(res)


def insert_child(ses: Session, study: Study, parent, rel: Relation, child):
    """
    Inserts child and connects it to the parent node.
    The attributes 'triggered_elements' and 'children' are omitted before Insert.
    These 2 attributes are lists, which causes exceptions in the database.

    :param ses: the Neo4j session
    :param parent: the parent node
    :param rel: the relationship
    :param child: the child node to be inserted
    """
    query = "MATCH path = shortestPath((s:Study)-[*1..25]->(parent:Widget|Wrap)) " \
            "WHERE s.studyName = $study_name AND s.studyVersion = $version " \
            "AND parent.id = $p_id " \
            f"MERGE (parent)-[:{rel.get_relation_type()} {attributes_to_string(rel)}]->" \
            f"(w:{child.get_category()} {attributes_to_string(child)})"
    values = {
        "study_name": study.get_study_name(),
        "version": study.get_study_version(),
        "p_id": parent.get_id()
    }
    values.update(rel.__dict__)
    values.update(child.__dict__)
    values = remove_attribute(values, "triggered_elements", "children")
    ses.run(query, values)


def get_children_of_form(ses: Session, study: Study, form: Form):
    """
    This query retrieves the immediate nodes of the form.

    :param ses:
    :param study:
    :param form:
    :return:
    """
    query = "MATCH (:Study {studyName: $study, studyVersion: $version})--(f:Form {formName: $form})-" \
            "[*1]->(w) RETURN f, w as children ORDER BY children.fieldId"
    values = {
        "study": study.get_study_name(),
        "version": study.get_study_version(),
        "form": form.get_form_name()
    }
    res = ses.run(query, values)
    return list(res)


def get_children_of_node(ses: Session, study: Study, identifier: str):
    # todo maybe refactor this to path, because of the better performance
    # study_name = identifier.split("/")[2]
    # query = "MATCH path = shortestPath((s:Study)-[*1..25]->(parent:Widget|Wrap)) " \
    #         "WHERE s.studyName = $name AND s.studyVersion = $version and parent.id = $identifier " \
    #         "MATCH (parent)-[*1]->(children:Widget|Wrap) " \
    #         "RETURN parent, children ORDER BY children.fieldId"
    query = "MATCH path = shortestPath((s:Study)-[*1..25]->(p:Widget|Wrap)) " \
            "WHERE s.studyName = $name AND s.studyVersion = $version AND p.id = $identifier " \
            "MATCH (p)-[r]->(child) " \
            "RETURN child as children"
    values = {
        "name": study.get_study_name(),
        "version": study.get_study_version(),
        "identifier": identifier
    }
    res = ses.run(query, values)
    return list(res)
