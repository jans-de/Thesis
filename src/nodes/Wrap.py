from nodes.Node import Node
from relations.Relation import Relation


class Wrap(Node):
    """
    Base class for Wraps.

    Attributes:
        relations (list[Relation]): stores the relations to child elements
    """
    relations: list[Relation]

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.children = properties.get("children")
        self.category = "Wrap"
        self.sbuild_type = self.build_type.replace("afForm_" + self.category + "_", "")

    def has_children(self) -> bool:
        """
        Determines if this Wrap has child elements.
        :return: False if len(self.children) is 0 or self.children is None, else True
        """
        return len(self.children) == 0 or self.children is not None

    def get_children(self) -> list[Node]:
        """
        Getter method for self.children

        :return: the children
        """
        return self.children

    def add_child(self, node: Node):
        """
        Appends a node to the list of children

        :param node: the child to append
        """
        self.children.append(node)

    def is_trigger_elem(self) -> bool:
        """
        Determines if this Wrap is a TriggerElement
        :return: True, if this element has a triggerElement, False otherwise
        """
        return True if self.get_dict().get("triggerElement") is not None else False


class WrapAnd(Wrap):
    """
    Dataclass for Wrap_And.
    """
    def __init__(self, properties: dict):
        super().__init__(properties)


class WrapOr(Wrap):
    """
    Dataclass for Wrap_Or.
    """
    def __init__(self, properties: dict):
        super().__init__(properties)


class WrapGroup(Wrap):
    """
    Dataclass for Wrap_Group_Inline and Wrap_Group_Outline.
    """
    def __init__(self, properties: dict):
        super().__init__(properties)


class WrapSlider(Wrap):
    """
    Dataclass for SliderWraps derived from the base class Wrap.

    Attributes:
        trigger_element (str): the element whose value controls the visibility of its children
        triggered_elements (list[Relation]): a list of Relation objects
    """
    trigger_element: str
    triggered_elements: list[Relation]

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.trigger_element = properties.get("triggerElement", "")
        self.triggered_elements = []

    def get_trigger_element(self) -> str:
        """
        Getter for attribute 'trigger_element'.

        :return: self.trigger_element
        """
        return self.trigger_element

    def add_triggered_element(self, trigger_elem: str, trigger_val: str):
        """
        Appends a new Relation with type "TRIGGERS" to the self.triggered_elements.

        :param trigger_elem: the field to be triggered
        :param trigger_val: the trigger value of the relation
        """
        self.triggered_elements.append(Relation(
            relation_type="TRIGGERS",
            dest=trigger_elem,
            on_value=trigger_val)
        )

    def get_triggered_elements(self):
        """
        Getter for the attribute 'triggered_elements'.

        :return: self.triggered_elements
        """
        return self.triggered_elements

    def get_triggered_relation_by_field_name(self, field_name) -> Relation:
        """
        Get the Relation from :py:attr:`triggered_elements`.

        :param field_name:
        :return: the Relation where the destination is :py:attr:`field_name`
        """
        for i in self.get_triggered_elements():
            if i.get_dest() == field_name:
                return i


def create_wrap(properties: dict) -> Wrap | None:
    """
    This function creates a new Wrap object from the given dictionary.
    If the buildType is None a Wrap is returned. If the buildType is not supported, returns None,
    otherwise an object of one of the Wrap subclasses, which are defined in this file.

    :param properties: the dictionary of properties
    :return: a Wrap node or None
    """
    build_type = properties.get("buildType")

    if build_type is None:
        node = Wrap(properties)
    else:
        # shorten build_type
        build_type = str(build_type).replace("afForm_Wrap_", "")

        # create Wraps
        if build_type == "And":
            node = WrapAnd(properties)
        elif build_type == "Or":
            node = WrapOr(properties)
        elif build_type == "Group_Inline" or build_type == "Group_Outline":
            node = WrapGroup(properties)
        elif build_type == "Slider_Multi":
            node = WrapSlider(properties)
            node.trigger_element = properties.get("triggerElement")
            triggers = properties.get("trigger", [])
            for t in triggers:
                on_value = t.get("value")
                for e in t.get("elements"):
                    node.add_triggered_element(e, on_value)
        else:
            print(f"build_type {build_type} is not supported")
            node = None

    return node
