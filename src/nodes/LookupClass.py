class LookupClass:
    """
    Data class for Lookups classes.
    A lookup class consisting of the name of the lookup class and an undefined amount of lookup definitions.
    The definitions are not part of the class variables, since they're different for every lookup class.
    Definitions are the options and values of the lookup entries.

    Attributes:
        name (str): the name of the lookup class
        definitions (dict): the lookup entries
    """
    name: str

    def __init__(self, properties: dict):
        self.name = properties.get("name", "")
        definitions = properties.get("definitions", {})
        # creates the definitions either from the dict value "definitions" or directly from the dict "properties"
        if len(definitions) != 0:
            for k, v in definitions.items():
                self.__setattr__(k, v)
        else:
            for k, v in properties.items():
                self.__setattr__(k, v)

    def get_name(self):
        """
        Getter for class variable `name`
        :return: self.name
        """
        return self.name

    def get_definition(self, name):
        """
        Getter for the lookup entry `name`.
        :param name: the key of the lookup entry
        :return: the value of the key `name`
        """
        return self.__dict__.get(name, "")

    def get_definitions(self):
        """
        Getter for all lookup entries.
        :return: a dictionary consisting of all attributes except "name"
        """
        return {k: v for k, v in self.__dict__.items() if not k == "name"}
