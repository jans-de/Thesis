from nodes import Widget, Wrap


def create_node(element: dict):
    # element_blacklist stores elements which are not necessary for the field_list generation
    element_blacklist = ["Wudo_Header", "Wudo_Subhead", "Wudo_Infobox"]

    element = {to_camel_case(k): v for k, v in element.items()}
    build_type = element.get("buildType")
    node = None

    if build_type is None:
        # buildType does not exist in element, like in forms
        return node
    elif build_type in element_blacklist:
        # buildType is not relevant for the data generation
        return node
    elif build_type.find("Wrap") != -1:
        node = Wrap.create_wrap(element)
    elif build_type.find("Widget") != -1:
        node = Widget.create_widget_node(element)

    return node


def attributes_to_string(elem) -> str:
    """
    This is a helper method, that returns the attributes of a Node or Relationship as a string in the form of:
    attribute: $attribute, where 'attribute' is camel cased. The first and last character of the string is
    '{' or '}' respectively.
    The purpose of this function is to keep the query string in the insert functions as dynamic as possible.

    Example:
    node = Node(form_name='ex', val='2') becomes "{form_name: $form_name, val: $val}"

    :param elem: a Node or Relation object, whose attributes should be converted into a string
    :return: an empty string if elem is None, otherwise the string of attributes
    """
    d = {}

    # None type causes error in below loop,
    if elem is None:
        return ""

    # get the properties of the element to a dictionary, then get its items, aka. a tuple[key, value]
    for i in elem.__dict__.items():
        # filter out the attributes 'triggered_elements' and 'children'
        # these elements are lists and cause error during insert into the db
        if i[0] != "triggered_elements" and i[0] != "children" and i[0] != "dest":
            # refactor each element to a dictionary in the format: {"anAttribute": $value}
            d.update({to_camel_case(i[0]): '$' + i[0]})
    # the element should be represented without apostrophes in the database
    tmp = str(d).replace("'", "")
    return tmp


def to_camel_case(snake_str: str) -> str:
    """
    A helper function to transform a snake-case string to camel case.
    This method is necessary, because Neo4j best convention is camel case, while python's is snake case.

    :param snake_str: the string to be converted
    :return: the given string in camel case
    """
    components = snake_str.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    # source: https://stackoverflow.com/questions/19053707/converting-snake-case-to-lower-camel-case-lowercamelcase
    return components[0] + ''.join(x.title() for x in components[1:])


def remove_attribute(values: dict, *args):
    """
    Removes the given items from the given dictionary and returns the updated dictionary.
    This function checks if the key exists, then utilizes dict.pop() to remove it.

    :param values: the dictionary to be updated
    :param args: a variable number of items to remove
    :return: the updated dictionary
    """
    for key in args:
        if values.get(key) is not None:
            values.pop(key)
    return values
