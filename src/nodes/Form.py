class Form:
    """
    Data class for forms.

    Attributes:
        form_name (str): the name of the form.
        form_type (str): this var controls if the form can be saved multiple times (i.e. it has a '1:n' relation)
            or once (1:1).
            The value may either be 'one' for 1:1 forms or 'many' for 1:n forms.
    """
    form_name: str
    form_type: str

    def __init__(self, form_name: str, form_type: str):
        self.form_name = form_name
        self.form_type = form_type

    def get_form_name(self) -> str:
        """
        Getter method for the variable `form_name`.

        :return: the form name
        """
        return self.form_name

    def get_form_type(self) -> str:
        """
        Getter method for the variable `relation_type`.

        :return: the type of relation
        """
        return self.form_type
