from nodes.Node import Node


class Widget(Node):
    """
    Base data class for Widgets.

    Attributes are stored in the dictionary `properties`, which is passed in the `__init__` method of the base-
        or subclass. Properties are accessed using :py:func:`typing.mapping.get`

    Inherits all attributes from its parent class :py:class:`Node`

    additional Attributes:
        category (str): constant value: 'Widget'
    """

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.category = "Widget"
        self.sbuild_type = self.build_type.replace("afForm_" + self.category + "_", "")


class WidgetNumber(Widget):
    """Data class for afForm_Widget_Integer and afForm_Widget_Float.\n

    Has the same attributes as the base class.

    additional Attributes:
        max_length (int): The maximum number of characters including '.' and ','. If missing: 1
        negative_value_allowed (bool): Are negative values allowed. If missing: False
        min (int | float): The minimum value of this field. If missing: 0
        max (int | float): The maximum value of this field. If missing: 1
    """
    max_length: int
    negative_value_allowed: bool
    min: int | float
    max: int | float

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.max_length = int(properties.get("maxLength", 1))
        self.negative_value_allowed = properties.get("negativeValueAllowed", False)
        self.min = int(properties.get("minValue", 0))
        self.max = int(properties.get("maxValue", 1))

    def get_min(self):
        return self.min

    def get_max(self):
        return self.max

    def get_negative_value_allowed(self):
        return self.negative_value_allowed

    def get_max_length(self):
        return self.max_length


class WidgetDate(Widget):
    """Data class for afForm_Widget_Date.\n

    Has the same attributes as the base class.

    additional Attributes:
        not_in_future (bool): may this date be in the future? If missing, True
    """
    not_in_future: bool

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.not_in_future = properties.get("notInFuture", True)

    def get_not_in_future(self):
        return self.not_in_future


class WidgetTime(Widget):
    """Data class for afForm_Widget_Date\n

    Has the same attributes as the base class.

    additional Attributes:
        # todo
    """

    def __init__(self, properties: dict):
        super().__init__(properties)


class WidgetDateTime(Widget):
    """
    todo
    """
    not_in_future: bool

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.not_in_future = properties.get("notInFuture", True)

    def get_not_in_future(self):
        return self.not_in_future


class WidgetRadio(Widget):
    """Data class for afForm_Widget_Radio\n

    Has the same attributes as the base class.

    additional Attributes:
        lookup_class (str): the class of options which are valid
    """
    lookup_class: str

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.lookup_class = properties.get("lookupClass", "").split("/")[-1]

    def get_lookup_class(self):
        return self.lookup_class


class WidgetLookup(Widget):
    """Data class for afForm_Widget_Lookup\n
    'lookup' is a synonym for 'dropdown menu'.

    Has the same attributes as the base class.

    additional Attributes:
        lookup_class (str): the class of options which are valid
    """
    lookup_class: str

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.lookup_class = properties.get("lookupClass", "").split("/")[-1]

    def get_lookup_class(self):
        return self.lookup_class


class WidgetCheckbox(Widget):
    """Data class for afForm_Widget_Checkbox\n
    A checkbox is an element that allows users to select or deselect an option.
    Checkboxes can only have the value 'empty' or 'filled'.

    Has the same attributes as the base class.
    """

    def __init__(self, properties: dict):
        super().__init__(properties)


class WidgetText(Widget):
    """Data class for afForm_Widget_Text\n

    Has the same attributes as the base class.

    additional Attributes:
        max_length (int): how long can the longest possible string be
    """
    max_length: int

    def __init__(self, properties: dict):
        super().__init__(properties)
        self.max_length = properties.get("maxLength", 1)

    def get_max_length(self):
        return self.max_length


def create_widget_node(properties: dict) -> Widget:
    """
    Creates a Widget object based on the property "buildType" from the given dictionary.

    :param properties: the dictionary, which represents the node.
    :exception AttributeError: when properties is None
    :return: an object of type Widget or one of its subclasses. None if the buildType is not supported.
    """
    build_type = properties.get("buildType")

    if build_type is None:
        # just in case, this case should not appear though
        raise Exception("buildType not found")
    else:
        node = None
        build_type = str(build_type).replace("afForm_Widget_", "")
        if build_type == "Date":
            node = WidgetDate(properties)
        elif build_type == "DateTime":
            node = WidgetDateTime(properties)
        elif build_type == "Time":
            node = WidgetTime(properties)
        elif build_type == "Integer" or build_type == "Float":
            node = WidgetNumber(properties)
        elif build_type == "Lookup":
            node = WidgetLookup(properties)
        elif build_type == "Radio":
            node = WidgetRadio(properties)
        elif build_type == "Checkbox":
            node = WidgetCheckbox(properties)
        elif build_type == "Text":
            node = WidgetText(properties)
        else:
            print(f"build_type {build_type} is unknown. Nothing done...")
    return node


# this code checks, if this module is run directly or if it is imported
# if __name__ == __main__, then the file is run directly
if __name__ == "__main__":
    print("Please run PDG.py instead. Exiting...")
    exit(0)
