class Study:
    """
    Base class for Studies.

    Attributes:
        system_name (str): the name of the system the study belongs to
        study_name (str): the name of the study
        study_title (str): the title of the study
        study_version (str): the version of the study
    """
    system_name: str
    study_name: str
    study_title: str
    study_version: str

    def __init__(self, system_name: str, study_name: str, study_title: str, study_version: str):
        self.system_name = system_name
        self.study_name = study_name
        self.study_title = study_title
        self.study_version = study_version

    def get_system_name(self) -> str:
        """
        Getter for system_name
        :return: self.system_name
        """
        return self.system_name

    def get_study_name(self) -> str:
        """
        Getter for study_name
        :return: self.study_name
        """
        return self.study_name

    def get_study_title(self) -> str:
        """
        Getter for study_title
        :return: self.study_title
        """
        return self.study_title

    def get_study_version(self) -> str:
        """
        Getter for study_version
        :return: self.study_version
        """
        return self.study_version
