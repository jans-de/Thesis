import nodes.NodeUtils as nutils


class Node:
    """
    Base class for Widgets and Wraps, because they share a couple of properties.

    Attributes:
        id (str): the unique identifier of this field.
            The id is structured according to the scheme: alcedis/<system>/<study>/<version>/form/<form>/e__n
            where n is a unique number for this form.
        build_type (str): the type of field, build_type is prepended by 'afForm_<category>_'. see also 'sbuild_type'
        name (str): the technical name of this field used in the eCRF
        mandatory(str): the mandatory of this field. A mandatory is an indicator whether this field must be filled
            (error), should be filled (warning) or is "nice to have" (optional)
        category (str): the category of this field. Teh value can only be "Widget" or "Wrap".
        sbuild_type (str): the s stands for short. sbuild_type is just the last name of the build_type, e.g. Radio
    """
    id: str
    name: str
    category: str
    build_type: str
    mandatory: str
    sbuild_type: str = ""

    def __init__(self, properties: dict):
        self.id = properties.get("id", "")
        self.name = properties.get("name", "")
        self.category = ""
        self.build_type = properties.get("buildType", "")
        self.mandatory = properties.get("mandatory", "optional")
        # sbuild type is implemented in the subclasses

    def get_id(self) -> str:
        return self.id

    def set_id(self, val):
        self.id = val

    def get_build_type(self) -> str:
        return self.build_type

    def get_sbuild_type(self) -> str:
        return self.sbuild_type

    def get_name(self) -> str:
        return self.name

    def get_mandatory(self) -> str:
        return self.mandatory

    def get_category(self) -> str:
        return self.category

    def has_children(self):
        return False

    def delattr(self, attr: str):
        """
        Delete the given attribute. Shadow method of :py:func:`dict.__delattr__()`

        :param attr: the attribute to be deleted
        """
        self.__delattr__(attr)

    def get_all_attributes(self) -> dict:
        """
        Get all attributes as a dictionary representation.
        Uses the built-in function __dict__()

        :return: a dictionary of all attributes
        """
        return self.get_dict()

    def get_attribute(self, attr: str) -> dict:
        """
        Dynamically get any attribute

        :param attr: the attribute to retrieve
        :return: the given attribute or an empty dict
        """
        return self.get_dict().get(attr, {})

    def get_dict(self) -> dict:
        return {nutils.to_camel_case(k): v for k, v in self.__dict__.items()}
