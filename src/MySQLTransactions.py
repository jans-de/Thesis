from nodes.Form import Form
from nodes.Node import Node
from nodes.Study import Study
from relations.Relation import Relation


def get_field_by_id(field_id: int):
    """
    SELECT action for table `Field` by field id.

    :param field_id:
    :return: all columns and rows of the given field_id
    """
    query = '''
        SELECT * FROM Fields
        WHERE id = %(fieldId)s;
    '''
    values = {"fieldId": field_id}
    return query, values


def get_field_by_name(field_name: str):
    """
    SELECT action for table `Field` by field form_name.
    This query requires the identifier (i.e. alcedis/system/study/version/form/field_name) since this query obtains
    the id from column `identifier` of the view `FullNames`. When executed, the query returns the id of the field.

    :param field_name: the identifier of the field
    :return: the if of the field
    """
    query = '''
        SELECT id FROM FullNames
        WHERE identifier LIKE %(fieldName)s;
    '''
    values = {"fieldName": field_name}
    return query, values


def insert_field_with_luc(form_id: int, luc_id: int, node: Node) -> tuple[str, dict]:
    query = '''
        INSERT INTO Fields (formId, lookupClassId, name, buildType, mandatory)
        VALUES (%(formId)s, %(lucId)s, %(name)s, %(buildType)s, %(mandatory)s);
    '''
    values = {
        "formId": form_id,
        "lucId": luc_id,
        "name": node.get_name(),
        "mandatory": node.get_mandatory(),
        "buildType": node.get_build_type()
    }
    return query, values


def insert_field(form_id: int, node: Node) -> tuple[str, dict]:
    query = '''
        INSERT INTO Fields (formId, name, buildType, mandatory)
        VALUES (%(formId)s, %(name)s, %(buildType)s, %(mandatory)s);
    '''
    values = {
        "formId": form_id,
        "name": node.get_name(),
        "mandatory": node.get_mandatory(),
        "buildType": node.get_build_type()
    }
    return query, values


def get_field(study: str, version: str, form: str, node: Node):
    query = '''
        SELECT 
            stud.name as studyName,
            stud.version as version,
            form.name as formName,
            Fields.name as fieldName,
            Fields.id as fieldId
        FROM Fields
        JOIN Forms form ON form.id = Fields.formId
        JOIN Studies stud ON stud.id = form.studyId
        WHERE stud.name LIKE %(studyName)s AND
            stud.version LIKE %(version)s AND
            form.name LIKE %(formName)s AND
            Fields.name LIKE %(fieldName)s;
    '''
    values = {
        "studyName": study,
        "version": version,
        "formName": form,
        "fieldName": node.get_name()
    }
    return query, values


def get_field_id_of_field(field_id: int):
    query = '''
        SELECT fullName
        FROM FullNames
        WHERE fieldId = %(fieldId)s;
    '''
    values = {"fieldId": field_id}
    return query, values


def get_children_of_form(study_id: int, form: Form):
    """
    Get all immediate children of this Form.
    The query achieves this by selecting all Fields where Relations.fromFieldId is not Relations.toFieldId

    :param study_id:
    :param form:
    :return:
    """
    query = '''
        SELECT Fields.id as fieldId, Fields.name as name, Fields.buildType as buildType, 
            Fields.mandatory as mandatory, lc.name as lookupClass, fn.identifier
        FROM Fields
        JOIN Forms f ON Fields.formId = f.id
        JOIN FullNames fn ON fn.fieldId = Fields.id
        LEFT JOIN LookupClasses lc ON Fields.lookupClassId = lc.classId
        WHERE f.studyId = %(studyId)s
            AND f.name LIKE %(formName)s
            AND Fields.id NOT IN (SELECT toFieldId FROM Relations)
        ORDER BY Fields.id;
    '''
    values = {
        "studyId": study_id,
        "formName": form.get_form_name()
    }
    return query, values


def get_children_of_field(field: str):
    """
    v2 might be faster
    :param field:
    :return:
    """
    # v1
    # query = '''
    #     SELECT Fields.*, fn2.identifier
    #     FROM Fields
    #     JOIN Relations ON Relations.toFieldId = Fields.id
    #     JOIN FullNames fn1 ON Relations.fromFieldId = fn1.fieldId
    #     JOIN FullNames fn2 ON Relations.toFieldId = fn2.fieldId
    #     WHERE fn1.identifier = 'Sponsor/study/1.0/patient/patient_group_pat_info';
    # '''
    # v2
    query = '''
        SELECT DISTINCT Fields.id as fieldId, Fields.name as name, Fields.buildType as buildType, 
            Fields.mandatory as mandatory, lc.name as lookupClass, fn2.identifier
        FROM Fields
        JOIN Relations R ON R.toFieldId = Fields.id
        JOIN FullNames fn2 ON R.toFieldId = fn2.fieldId
        LEFT JOIN LookupClasses lc on lc.classId = Fields.lookupClassId 
        WHERE R.fromFieldId IN (
            SELECT fieldId
            FROM FullNames
            WHERE identifier = %(fieldName)s
        );
    '''
    values = {
        "fieldName": field
    }
    return query, values


def get_field_attributes_by_id(field_id):
    """
    SELECT action for the relation FieldAttributes.

    :param field_id: the id of the field
    :return: all columns and rows from FieldAttribute of `field_id`
    """
    query = '''
        SELECT * FROM FieldAttributes
        WHERE fieldId = %(field_id)s;
    '''
    values = {"field_id": field_id}
    return query, values


def insert_field_attributes(field_id: int, attribute, value):
    query = '''
        INSERT INTO FieldAttributes (fieldId, attribute, value)
        VALUES (%(fieldId)s, %(attribute)s, %(value)s);
    '''
    values = {
        "fieldId": field_id,
        "attribute": attribute,
        "value": value
    }
    return query, values


def get_forms(study_id: int):
    query = """
        SELECT * FROM Forms
        WHERE Forms.studyId LIKE %(studyId)s;
    """
    values = {"studyId": study_id}
    return query, values


def get_form_by_name(study_id, form_name: str):
    """
    This query retrieves one row from Forms which meet the parameter `form_name` which belongs
    to the given `study_id`.

    :param study_id: the study id of the desired form
    :param form_name: the name of the desired form
    :returns: query and values
    """
    query = '''
        SELECT * FROM Forms
        WHERE studyId = %(studyId)s AND name LIKE %(formName)s;
    '''
    values = {
        "studyId": study_id,
        "formName": form_name
    }
    return query, values


def get_forms_for_study(study_id: int):
    """
    This query retrieves all entries from `Forms` which meet the parameter `study_id`.

    :param study_id: the id of the study
    :return: all rows and columns for the given study_id
    """
    query = '''
        SELECT * FROM Forms
        WHERE studyId = %(studyId)s;
    '''
    values = {"studyId": study_id}
    return query, values


def insert_form(study_id, form: Form):
    """
    This query inserts a form based on the values of the parameter `form`.

    :param study_id: the id of the study
    :param form: the form node to be inserted
    """
    query = '''
        INSERT INTO Forms (studyId, name, type)
        VALUES (%(studyId)s, %(name)s, %(formType)s);'''
    values = {
        "studyId": study_id,
        "name": form.get_form_name(),
        "formType": form.get_form_type()
    }
    return query, values


def insert_lookup_class(last_luc_id: int, name: str, entry: str, value: str):
    """
    This query inserts a lookup entry (one row). This func has to be called for
    every option of the lookup.

    :param last_luc_id: the lookupClassId
    :param name: name of the lookup
    :param entry: an option
    :param value: the value of entry
    """
    query = '''
        INSERT INTO LookupClasses (classId, name, entry, value)
        VALUES (%(lastLucId)s, %(name)s, %(entry)s, %(value)s);
    '''
    values = {
        "lastLucId": last_luc_id,
        "name": name,
        "entry": entry,
        "value": value
    }
    return query, values


def get_luc_by_name(luc_name: str) -> [str, str]:
    """
    This query retrieves all entries of `LookupClasses` which match the parameter.

    :param luc_name: the name of the Lookup
    :return: query and value
    """
    query = '''
        SELECT classId, name, entry, value FROM LookupClasses
        WHERE name LIKE %(lucName)s;
    '''
    values = {"lucName": luc_name}
    return query, values


def insert_relation(from_field_id, to_field_id, relation: Relation):
    query = '''
    INSERT INTO Relations(fromFieldId, toFieldId, relationType, onValue)
    VALUES (%(fromFieldId)s, %(toFieldId)s, %(relType)s, %(onValue)s);'''
    values = {
        "fromFieldId": from_field_id,
        "toFieldId": to_field_id,
        "relType": relation.get_relation_type(),
        "onValue": relation.get_on_value()
    }
    return query, values


def get_relation_attributes_by_relation_id(rel_id):
    query = '''
        SELECT * FROM RelationAttributes
        WHERE fieldRelationId = %(relId)s;
    '''
    values = {"relId": rel_id}
    return query, values


def insert_relation_attribute(rel_id, attribute, value):
    query = '''
        INSERT INTO RelationAttributes(fieldRelationsId, attribute, value)
        VALUES (%(relId)s, %(attribute)s, %(value)s);
    '''
    values = {
        "relId": rel_id,
        "attribute": attribute,
        "value": value
    }
    return query, values


def get_study_by_id(study_id: int):
    query = '''
        SELECT * FROM Studies
        WHERE id = %(studyId)s;
        '''
    values = {"studyId": study_id}
    return query, values


def get_study_id_by_node(study: Study):
    query = '''
        SELECT id FROM Studies
        WHERE name LIKE %(studyName)s;
        '''
    values = {"studyName": study.get_study_name()}
    return query, values


def insert_study(study: Study):
    query = '''
        INSERT INTO Studies (systemName, name, title, version) 
        VALUES (%(systemName)s, %(name)s, %(title)s, %(version)s);'''
    values = {
        "systemName": study.get_system_name(),
        "name": study.get_study_name(),
        "title": study.get_study_title(),
        "version": study.get_study_version()
    }
    return query, values


def get_parent_node_id_by_fullname(node: Node):
    query = '''
        SELECT from_field_id FROM Relations
        JOIN FullNames f on f.id = Relations.toFieldId
        WHERE f.identifier LIKE %(name)s;
    '''
    values = {"name": node.get_id()}
    return query, values
