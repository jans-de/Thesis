import timeit

from ADLReader import ADLReader
from CLIReader import CLIReader
from GeneratorController import GeneratorController


class PDG:

    def main(self, args: dict = None):
        if args is None:
            cli_reader = CLIReader()
            args = cli_reader.read_cli()

        if args.get("--study") is None:
            # if file is present, start in preparation mode
            reader = ADLReader(args)
            reader.read_adl_from_file(args)

        else:
            # starts usage mode
            gen_controller = GeneratorController(args)
            time = gen_controller.main()
            print(time)


if __name__ == "__main__":
    c = PDG()
    c.main()
    print("Finished")
    quit(1)
